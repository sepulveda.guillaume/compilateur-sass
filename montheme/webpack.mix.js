let mix = require('laravel-mix');

mix.sass('assets/scss/style.scss', 'assets/dist').options({
    processCssUrls: false,
});